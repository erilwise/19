﻿#include <iostream>

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "text" << std::endl;
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof \n" << std::endl;
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow \n" << std::endl;
	}
};

class Cow : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Moo \n" << std::endl;
	}
};

int main()
{
	const int size = 3;
	Animal* arrAnimal[size] = {new Dog, new Cat, new Cow };

	for (int i = 0; i < size; i++)
	{
		arrAnimal[i]->Voice();
	}
}

